mysql -u root -p

CREATE DATABASE dbrelational;

USE dbrelational;

CREATE TABLE author(
au_id VARCHAR(50) NOT NULL,
au_lname VARCHAR(50) NOT NULL,
au_fname VARCHAR(50) NOT NULL,
address VARCHAR(50) NOT NULL,
city VARCHAR(50) NOT NULL,
state VARCHAR (2) NOT NULL,
PRIMARY KEY(au_id)
);

CREATE TABLE publisher(
pub_id int NOT NULL,
pub_name VARCHAR(50) NOT NULL,
city VARCHAR(50) NOT NULL,
PRIMARY KEY(pub_id)
);

CREATE TABLE title(
title_id VARCHAR(50) NOT NULL,
title VARCHAR(50) NOT NULL,
type VARCHAR(50) NOT NULL,
price DOUBLE,
pub_id INT NOT NULL,
PRIMARY KEY(title_id),
CONSTRAINT fk_title_pub_id
    FOREIGN KEY(pub_id) REFERENCES publisher(pub_id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

CREATE TABLE author_title(
au_id VARCHAR(50) NOT NULL,
title_id VARCHAR(50) NOT NULL,
CONSTRAINT fk_author_title_au_id
    FOREIGN KEY (au_id) REFERENCES author(au_id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
CONSTRAINT fk_author_title_title_id
    FOREIGN KEY (title_id) REFERENCES title(title_id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);
--A. List the books Authored by Marjorie Green.
-- - The Busy Executive's Database Guide
-- - You Can Combat Computer Stress!
--B. List the books Authored by Michael O'Leary.
-- - Cooking with Computers
--C. Write the author/s of "The Busy Executives Database Guide".
-- - Marjorie Green, Abraham Bennet
--D. Identify the publisher of "But Is It User Friendly?".
-- - Algodata Infosystems
--E. List the books published by Algodata Infosystems.
-- - But Is It User Friendly, Secrets of Silicon Valley,
-- Net Etiquette, The Busy Executive's Database Guide, Cooking with Computers,
-- Straight Talk About Computers

mysql -u root -p

CREATE DATABASE blog_db;

USE blog_db;

CREATE TABLE users(
id INT NOT NULL,
email VARCHAR(100) NOT NULL,
password VARCHAR(300) NOT NULL,
datetime_created DATETIME NOT NULL,
PRIMARY KEY(id)
);

CREATE TABLE posts(
id INT NOT NULL,
author_id INT NOT NULL,
title VARCHAR(500) NOT NULL,
content VARCHAR(5000) NOT NULL,
datetime_posted DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_posts_author_id
    FOREIGN KEY (author_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

CREATE TABLE posts_comments(
id INT NOT NULL,
post_id INT NOT NULL,
user_id INT NOT NULL,
content VARCHAR(5000) NOT NULL,
datetime_commented DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_posts_comments_post_id
    FOREIGN KEY (post_id) REFERENCES posts(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
CONSTRAINT fk_posts_comments_user_id
    FOREIGN KEY (user_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

CREATE TABLE posts_likes(
id INT NOT NULL,
post_id INT NOT NULL,
user_id INT NOT NULL,
datetime_liked DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_posts_likes_post_id
    FOREIGN KEY (post_id) REFERENCES posts(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
CONSTRAINT fk_posts_likes_user_id
    FOREIGN KEY (user_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);
